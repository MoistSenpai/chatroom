// Install dependencies
const express = require('express');
const socketio = require('socket.io');
const colors = require('colors');

// Initanlize express and set port
const app = express();
const port = 3000;

// Initzalize http and socketio
const http = require('http').Server(app);
const socket = socketio(http);

// Send index.html to express
app.get('/', (req, res) => {
	res.sendFile(`${__dirname}/index.html`);
});

// Listen to events from socketio
socket.on('connection', (sock) => {
	// User connected
	console.log(`[Connection]`.green + ` -> User Connected`);
	// User disconnected
	sock.on('disconnect', () => console.log(`[Connection]`.red + ` -> User Disconnected`));
	// User sent a message (got from form)
	sock.on('message', (msg) => {
		// Log message
		console.log(`[${msg.username}]`.yellow + ` -> ${msg.m} `);
		// Broadcast message
		socket.emit('message', msg);
	});

});

// Listen on port and log
http.listen(port, () => {
	console.log(`[Server]`.magenta + ` -> Server listening on port ${port}`);
});